jQuery ($) ->
    # Put jquery functions in here.
    # To prevent the automatic returning of the last expression, use an empty return


    ######## This allows you to ignore the CSRF token for AJAX views, but it will be
    # added as a request header.
    # See: https://docs.djangoproject.com/en/dev/ref/contrib/csrf/#ajax
    getCookie = (name) ->
        cookieValue = null
        if document.cookie and document.cookie != ''
            cookies = document.cookie.split(';')
            for i in cookies
                cookie = jQuery.trim(cookies[i])
                # Does this cookie string begin with the name we want?
                if cookie.substring(0, name.length + 1) is (name + '=')
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
                    break;

        return cookieValue;

    csrftoken = getCookie('csrftoken');


    ####### Some useful ajax functions: http://brack3t.com/ajax-and-django-views.html
    apply_form_field_error = (fieldname, error) ->
        input = $("#id_" + fieldname)
        container = $("#div_id_" + fieldname)
        error_msg = $("<span />").addClass("help-inline ajax-error").text(error[0])

        container.addClass("error")
        error_msg.insertAfter(input)
        return

    clear_form_field_errors = (form) ->
        $(".ajax-error", $(form)).remove()
        $(".error", $(form)).removeClass("error")
        return


    django_message = (msg, level) ->
        levels = {
            warning: 'alert',
            error: 'error',
            success: 'success',
            info: 'info'
        }
        source = $('#message_template').html()
        template = Handlebars.compile(source)
        context = {
            'tags': levels[level],
            'message': msg
        }
        html = template(context)

        $("#message_area").append(html)
        return

    django_block_message = (msg, level) ->
        source = $("#message_block_template").html()
        template = Handlebars.compile(source)
        context = {level: level, body: msg}
        html = template(context)

        $("#message_area").append(html)
        return


    return


######## Avoid `console` errors in browsers that lack a console.
() ->
    noop = () ->
    methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ]
    length = methods.length
    console = (window.console = window.console || {})

    while length--
        method = methods[length]

        # Only stub undefined methods.
        if not console[method]
            console[method] = noop

