# -*- coding: utf-8 -*-

# Only authenticated users can access views using this.
from braces.views import LoginRequiredMixin, PrefetchRelatedMixin

# Import the customized User model
from .models import User
