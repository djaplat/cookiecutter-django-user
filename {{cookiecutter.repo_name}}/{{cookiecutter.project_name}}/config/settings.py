"""
For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

from unipath import Path
from configurations import Configuration, values

PROJECT_DIR = Path(__file__).ancestor(2)


class Common(Configuration):
    # Internationalization
    # https://docs.djangoproject.com/en/1.6/topics/i18n/

    # Hosts/domain names that are valid for this site; required if DEBUG is False
    # See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
    ALLOWED_HOSTS = []

    # Local time zone for this installation. Choices can be found here:
    # http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
    # although not all choices may be available on all operating systems.
    # In a Windows environment this must be set to your system time zone.
    TIME_ZONE = 'Europe/Amsterdam'

    # Language code for this installation. All choices can be found here:
    # http://www.i18nguy.com/unicode/language-identifiers.html
    LANGUAGE_CODE = 'en-us'

    SITE_ID = 1

    # If you set this to False, Django will make some optimizations so as not
    # to load the internationalization machinery.
    USE_I18N = False

    # If you set this to False, Django will not format dates, numbers and
    # calendars according to the current locale.
    USE_L10N = True

    # If you set this to False, Django will not use timezone-aware datetimes.
    USE_TZ = True

    ADMINS = (
        '{{cookiecutter.author_name}}', '{{cookiecutter.email}}'
    )

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
    MANAGERS = ADMINS

    ########## FIXTURE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
    FIXTURE_DIRS = (
        PROJECT_DIR.child('fixtures'),
    )

    ########## TEMPLATE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
    TEMPLATE_CONTEXT_PROCESSORS = (
        "django.contrib.auth.context_processors.auth",
        "django.core.context_processors.debug",
        "django.core.context_processors.i18n",
        "django.core.context_processors.media",
        "django.core.context_processors.static",
        "django.core.context_processors.tz",
        "django.contrib.messages.context_processors.messages",
        'django.core.context_processors.request',
    )

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
    TEMPLATE_DIRS = (
        PROJECT_DIR.child('templates')
    )

    TEMPLATE_LOADERS = (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )

    ########## STATIC FILE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
    STATIC_ROOT = PROJECT_DIR.child('staticfiles')

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
    STATIC_URL = '/static/'

    # See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
    STATICFILES_DIRS = (
        PROJECT_DIR.child('static'),
    )

    # See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    )

    ########## MEDIA CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
    MEDIA_ROOT = PROJECT_DIR.child('media')

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
    MEDIA_URL = '/media/'

    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = values.BooleanValue(False)
    TEMPLATE_DEBUG = values.BooleanValue(DEBUG)

    # Application definition

    DJANGO_APPS = (
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
    )

    THIRD_PARTY_APPS = (
        'django_extensions',
        'south',
        'django_assets',
    )

    PROJECT_APPS = (
        'users',
    )

    INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + PROJECT_APPS


    MIDDLEWARE_CLASSES = (
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    )

    ROOT_URLCONF = 'config.urls'

    WSGI_APPLICATION = 'config.wsgi.application'

    AUTH_USER_MODEL = 'users.User'

    ########## EMAIL CONFIGURATION
    EMAIL_BACKEND = values.Value('django.core.mail.backends.smtp.EmailBackend')

    DEFAULT_FROM_EMAIL = values.Value(
            '{{cookiecutter.author_name}} <{{cookiecutter.email}}>')

    ########## Webassets configuration
    # See: http://elsdoerfer.name/docs/django-assets/settings.html
    ASSETS_MODULES = [
        'config.assets'
    ]


class Local(Common):
    # SECURITY WARNING: keep the secret key used in production secret!
    SECRET_KEY = values.Value('CHANGEME!!!!')

    # Quick-start development settings - unsuitable for production
    # See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/
    INSTALLED_APPS = Common.INSTALLED_APPS + (
        'debug_toolbar',
        'django_outbox',
    )

    ########## Mail settings
    # Using django-outbox to test email
    # See: https://github.com/poiati/django-outbox
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = '/tmp/{{cookiecutter.project_name}}'

    ########## Webassets configuration
    ASSETS_ROOT = PROJECT_DIR.child('static')
    ASSETS_DEBUG = True
    # enable debug info in sass: http://snugug.com/musings/debugging-sass-source-maps
    SASS_DEBUG_INFO = True

    DB_NAME = values.Value('{{cookiecutter.project_name}}').default

    DATABASES = {
        'default': {
            'ENGINE':'django.db.backends.sqlite3',
            'NAME': '{0}.sqlite3'.format(DB_NAME),
        }
    }


    ######## Database setings
    # https://docs.djangoproject.com/en/1.6/ref/settings/#databases
    # Uses dj-database-url, see: https://github.com/kennethreitz/dj-database-url
    DATABASES = values.DatabaseURLValue(
        'postgres://{{cookiecutter.author_name}}@localhost/{{cookiecutter.project_name}}'
    )

    ########## django-debug-toolbar
    # Add the debug_toolbar middleware to the front of MIDDLEWARE_CLASSES
    MIDDLEWARE_CLASSES = ('debug_toolbar.middleware.DebugToolbarMiddleware', ) \
        + Common.MIDDLEWARE_CLASSES

    INTERNAL_IPS = values.IPValue('127.0.0.1',)

    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
        'SHOW_TEMPLATE_CONTEXT': True,
    }


class Production(Common):
    # SECURITY WARNING: keep the secret key used in production secret!
    SECRET_KEY = values.SecretValue()

    ########## Database settings
    # In production, add the full database-url value to the environment variables
    DATABASES = values.DatabaseURLValue()

    ########## EMAIL
    EMAIL_HOST = values.Value('smtp.sendgrid.com')
    EMAIL_HOST_PASSWORD = values.SecretValue(environ_prefix="", environ_name="SENDGRID_PASSWORD")
    EMAIL_HOST_USER = values.SecretValue(environ_prefix="", environ_name="SENDGRID_USERNAME")
    EMAIL_PORT = values.IntegerValue(587, environ_prefix="", environ_name="EMAIL_PORT")
    EMAIL_SUBJECT_PREFIX = values.Value('[{{cookiecutter.project_name}}] ',
        environ_name="EMAIL_SUBJECT_PREFIX")
    EMAIL_USE_TLS = True
    SERVER_EMAIL = EMAIL_HOST_USER

    ########## TEMPLATE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
    TEMPLATE_LOADERS = (
        ('django.template.loaders.cached.Loader', (
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        )),
    )

    # Webassets configuration
    # Automatically building asset bundles disabled in production
    # run `$ ./manage.py assets build` to build your assets in production
    ASSETS_DEBUG = False
    ASSETS_AUTO_BUILD = False
    ASSETS_MANIFEST = "cache"

    ########## django-secure
    # http://django-secure.readthedocs.org/en/v0.1.2/
    INSTALLED_APPS = Common.INSTALLED_APPS + ("djangosecure", )

    MIDDLEWARE_CLASSES = Common.MIDDLEWARE_CLASSES + (
        "djangosecure.middleware.SecurityMiddleware",)

    # Set to True if you are using SSL
    SECURE_SSL_REDIRECT = values.BooleanValue(False)
    # If you are using SSL; set this to 60 seconds and then to 518400
    # when you can prove it works
    SECURE_HSTS_SECONDS = values.IntegerValue(0)
    SECURE_HSTS_INCLUDE_SUBDOMAINS = values.BooleanValue(True)
    SECURE_FRAME_DENY = values.BooleanValue(True)
    SECURE_CONTENT_TYPE_NOSNIFF = values.BooleanValue(True)
    SECURE_BROWSER_XSS_FILTER = values.BooleanValue(True)
    SESSION_COOKIE_SECURE = values.BooleanValue(False)
    SESSION_COOKIE_HTTPONLY = values.BooleanValue(True)


class Testing(Common):
    ###### Testing settings
    # run with: ./manage.py test --configuration=Testing
    import os
    os.environ['REUSE_DB'] = "1"

    SECRET_KEY = 'SECRET_TESTING_KEY!!!'

    DATABASES = values.DatabaseURLValue(
        'postgres://{{cookiecutter.author_name}}@localhost/{{cookiecutter.project_name}}{0}'.format(
            '_test'
        )
    )

    INSTALLED_APPS = Common.INSTALLED_APPS + (
        'django_nose',
    )

    ########## Nose testing settings
    TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
    # for all options, see: https://nose.readthedocs.org/en/latest/usage.html?highlight=#options
    NOSE_ARGS = ['--stop', '--all-modules', '--nocapture', '--verbosity=2']

