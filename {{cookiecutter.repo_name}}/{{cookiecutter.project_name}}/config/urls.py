# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import patterns, include, url

from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^users/', include("users.urls", namespace="users")),

    # You should serve robots.txt directly through nginx:
    #
    # location  /robots.txt {
    #     alias  /path/to/robots.txt;
    # }
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    url(r'^humans\.txt$', TemplateView.as_view(template_name='humans.txt', content_type='text/plain')),
    url(r'^crossdomain\.xml$', TemplateView.as_view(template_name='crossdomain.xml', content_type='text/xml')),
)


if settings.DEBUG:
    import debug_toolbar
    from django.conf.urls.static import static
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^outbox/', include('django_outbox.urls')),
    ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
