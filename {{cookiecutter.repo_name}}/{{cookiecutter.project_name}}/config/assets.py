# -*- coding: utf-8 -*-
from django_assets import Bundle, register

# Both Coffeescript and UglifyJS require Node.
# For installation of Node see: http://howtonode.org/how-to-install-nodejs

# Sass requires Ruby to be installed. See: https://www.ruby-lang.org/en/

# For the installation of Coffeescript, see: http://jashkenas.github.io/coffee-script/#top
coffee = Bundle('coffee/script.coffee',
                filters='coffeescript', output='js/script.js')

# For installation of UglifyJS, see: https://github.com/mishoo/UglifyJS#install-npm
js = Bundle(coffee,
            filters='uglifyjs', output='js/script.js')

# For installation of Sass, see: http://sass-lang.com/install
css = Bundle('sass/styles.scss',
             filters='scss', output='css/styles.css', depends=('**/*.scss'))

register('js_all', js)
register('css_all', css)
