cookiecutter-django-user
========================

A cookiecutter_ template for Django.

.. _cookiecutter: https://github.com/audreyr/cookiecutter

Features
---------

* Custom User model
* Bourbon, Neat and Bitters Sass libraries
* Settings management via django-configurations
* Webassets for static assets management

Constraints
-----------

* Only maintained 3rd party libraries are used.
* Environment variables for configuration (This won't work with Apache/mod_wsgi).

Requirements
------------
Python_
cookiecutter_
Ruby_
Sass_ > gem install sass
Bourbon_, Neat_ and Bitters_ > gem install bourbon neat bitters



Usage
------

Let's pretend you want to create a Django project called "redditclone". Rather than using `startproject`
and then editing the results to include your name, email, and various configuration issues that always get forgotten until the worst possible moment, get cookiecutter_ to do all the work.

First, create a virtualenv and get cookiecutter.::

    $ pip install cookiecutter

Now run it against this repo::

    $ cookiecutter git@bitbucket.org:djaplat/cookiecutter-django-user.git

You'll be prompted for some questions, answer them, then it will create a Django project for you.


Local installation
------------------

Assuming you are using Virtualenvwrapper_; activate the virtualenv and install the requirements::

    $ workon your_virtualenv_name
    $ cd {{cookiecutter.repo_name}}
    $ pip install -U -r requirements/local.txt

To add the required environment variables, you can edit the postactivate file.
::

    $ vim $VIRTUAL_ENV/bin/postactivate

Make sure you add at least the SECRET_KEY (you can use the included django-extensions to generate a secret key by running $ python {{cookiecutter.project_name}}/manage.py generate_secret_key).

::

    export DJANGO_SECRET_KEY='the generated secret key'


Configuration
-------------
cookiecutter-django-user relies extensively on environment settings which **will not work with Apache/mod_wsgi setups**. Gunicorn/Nginx or uWSGI/Nginx should not be a problem.
For configuration purposes, cookiecutter-django-user relies on django-configurations_. Django-configurations looks for an environment variable with the same name as the Value variables in settings.py, only uppercased and prefixed with `DJANGO_`. E.g. when using something like ROOT_URLCONF = values.Value('mysite.urls'),
django-configurations will try to read the DJANGO_ROOT_URLCONF environment variable when deciding which value the ROOT_URLCONF setting should have

the following table maps the cookiecutter-django environment variables to their Django setting:


=========================== ================================================ ===========================================
Django Setting              Development Default                              Production Default
=========================== ================================================ ===========================================
DATABASES                   See code                                         See code
DEBUG                       True                                             False
EMAIL_BACKEND               django.core.mail.backends.filebased.EmailBackend django.core.mail.backends.smtp.EmailBackend
SECRET_KEY                  CHANGEME!!!                                      raises error
SECURE_HSTS_SECONDS         n/a                                              0
SECURE_BROWSER_XSS_FILTER   n/a                                              True
SECURE_SSL_REDIRECT         n/a                                              False
SECURE_CONTENT_TYPE_NOSNIFF n/a                                              True
SECURE_FRAME_DENY           n/a                                              True
HSTS_INCLUDE_SUBDOMAINS     n/a                                              True
SESSION_COOKIE_HTTPONLY     n/a                                              True
SESSION_COOKIE_SECURE       n/a                                              False
EMAIL_HOST                  n/a                                              smtp.sendgrid.com
SENDGRID_PASSWORD           n/a                                              raises error
SENDGRID_USERNAME           n/a                                              raises error
EMAIL_PORT                  n/a                                              587
EMAIL_SUBJECT_PREFIX        n/a                                              'project_name'
=========================== ================================================ ===========================================


User model
----------
cookiecutter-django-user comes with a custom user model. The included model doesn't actually differ much from the default Django user model. The only changes are that longer email addresses are allowed, and the users email is shown in the admin list view by default.
The provided user model is based on `this blog post`_, and provides a convenient starting point for customisation.


Static files
------------
Webassets_ is used to merge and compress Javascript and to compile Sass_. In order to be able to use Sass_ in your project, make sure you have installed Ruby_ and the Sass compiler.

::

    $ gem install sass

Contrary to the default settings in Webassets_, this project adds an assets.py file in the root of your project nad not in one of your apps.

The project uses Bourbon_, Neat_ and Bitters_. To use these, run the following commands to install and make sure everything is up to date

::

    $ gem install bourbon neat bitters
    $ cd {{cookiecutter.project_name}}/static/sass
    $ bourbon update && neat update && bitters update


Not Exactly What You Want?
---------------------------

This is what I want. *It might not be what you want.* Don't worry, you have options:

Fork This
~~~~~~~~~~

If you have differences in your preferred setup, I encourage you to fork this to create your own version.
Once you have your fork working, let me know and I'll add it to a '*Similar Cookiecutter Templates*' list here.
It's up to you whether or not to rename your fork.

If you do rename your fork, I encourage you to submit it to the following places:

* cookiecutter_ so it gets listed in the README as a template.
* The cookiecutter grid_ on Django Packages.

Or Submit a Pull Request
~~~~~~~~~~~~~~~~~~~~~~~~~

I also accept pull requests on this, if they're small, atomic, and if they make my own project development
experience better.

.. _django-configurations: http://django-configurations.readthedocs.org/en/latest/
.. _cookiecutter: https://github.com/audreyr/cookiecutter
.. _grid: https://www.djangopackages.com/grids/g/cookiecutter/
.. _Bourbon: http://bourbon.io/
.. _Neat: http://neat.bourbon.io/
.. _Bitters: https://github.com/thoughtbot/bitters
.. _virtualenvwrapper: http://virtualenvwrapper.readthedocs.org/en/latest/
.. _webassets: https://github.com/miracle2k/webassets
.. _sass: http://sass-lang.com/
.. _Ruby: https://www.ruby-lang.org/en/
.. _this blog post: http://www.caktusgroup.com/blog/2013/08/07/migrating-custom-user-model-django/